# Copyright 1999-2009 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="2"

DESCRIPTION="Repository of data files describing media player capabilities."
HOMEPAGE="http://people.freedesktop.org/~teuf/media-player-info/"
SRC_URI="http://people.freedesktop.org/~teuf/${PN}/${P}.tar.gz"

LICENSE="BSD"
SLOT="0"
KEYWORDS="~x86"
IUSE=""

DEPEND=">=sys-fs/udev-145[extras]"
RDEPEND="${DEPEND}"

src_install() {
        emake DESTDIR="${D}" install || die "emake install failed"

	dodoc AUTHORS ChangeLog NEWS README || die "installing docs failed"
}
