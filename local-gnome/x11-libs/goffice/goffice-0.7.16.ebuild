# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/x11-libs/goffice/goffice-0.6.5.ebuild,v 1.2 2008/09/28 07:59:23 aballier Exp $

EAPI=2

inherit eutils gnome2 flag-o-matic

DESCRIPTION="A library of document-centric objects and utilities"
HOMEPAGE="http://freshmeat.net/projects/goffice/"

LICENSE="GPL-2"
SLOT="0.8"
KEYWORDS="~x86"
IUSE="doc gconf gnome"

#FIXME Add optional dep on lasem (http://www.ohloh.net/p/Lasem)

RDEPEND=">=dev-libs/glib-2.16
	gconf? ( gnome-base/gconf )
	>=gnome-extra/libgsf-1.14.9[gnome]
	>=dev-libs/libxml2-2.4.12
	>=x11-libs/pango-1.8.1
	>=x11-libs/gtk+-2.12
	>=x11-libs/cairo-1.2[svg]"

DEPEND="${RDEPEND}
	>=dev-util/pkgconfig-0.18
	>=dev-util/intltool-0.35
	doc? ( >=dev-util/gtk-doc-1.4 )"

DOCS="AUTHORS BUGS ChangeLog MAINTAINERS NEWS README"

pkg_setup() {
	G2CONF="${G2CONF}
		$(use_with gnome)
		$(use_with gconf)"
}

src_compile() {
	filter-flags -ffast-math
	gnome2_src_compile
}
