EAPI="2"

inherit eutils gnome2

DESCRIPTION="Fully featured yet light and fast cross platform word processor"
HOMEPAGE="http://www.abisource.com/"
SRC_URI="http://www.abisource.com/downloads/${PN}/${PV}/source/${P}.tar.gz"

LICENSE="GPL-2"
SLOT="2"
KEYWORDS="~alpha ~amd64 ~hppa ~ia64 ~ppc ~ppc64 ~sparc ~x86"
IUSE="debug gnome spell"

# FIXME: gsf could probably be conditional

RDEPEND="dev-libs/popt
	sys-libs/zlib
	>=dev-libs/glib-2.18
	>=x11-libs/gtk+-2.12
	>=x11-libs/pango-1.24.2[X]
	>=gnome-base/librsvg-2.16
	>=x11-libs/goffice-0.7:0.8
	media-libs/jpeg
	media-libs/libpng
	>=app-text/wv-1.2
	>=dev-libs/fribidi-0.10.4
	spell? ( >=app-text/enchant-1.2 )
	gnome?	( >=gnome-extra/gucharmap-1.4 )
	>=gnome-extra/libgsf-1.12.0"

DEPEND="${RDEPEND}
		>=dev-util/pkgconfig-0.9"

pkg_setup() {
	G2CONF="${G2CONF}
		$(use_enable debug)
		$(use_with gnome gucharmap)
		$(use_with gnome gio)
		$(use_with gnome goffice)
		$(use_enable spell spell)
		--without-gnomevfs
		--disable-static"
}

src_prepare() {
	gnome2_src_prepare

	# Fix build with goffice:0.8
	epatch "${FILESDIR}/${P}-missing-defines.patch"
}
