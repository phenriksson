# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/app-office/gnumeric/gnumeric-1.8.3.ebuild,v 1.9 2008/09/28 07:59:43 aballier Exp $

EAPI=2

inherit gnome2 flag-o-matic

DESCRIPTION="Gnumeric, the GNOME Spreadsheet"
HOMEPAGE="http://www.gnome.org/projects/gnumeric/"
LICENSE="GPL-2"

SLOT="0"
KEYWORDS="~x86"

IUSE="gconf gnome perl python"
# bonobo guile libgda mono (experimental)

# lots of missing files, wait for next release
RESTRICT="test"

RDEPEND="sys-libs/zlib
	app-arch/bzip2
	>=dev-libs/glib-2.12
	>=gnome-extra/libgsf-1.14.15[gnome]
	>=x11-libs/goffice-0.7.16
	>=dev-libs/libxml2-2.4.12
	x11-libs/cairo[svg]
	>=x11-libs/pango-1.12

	>=x11-libs/gtk+-2.12
	>=gnome-base/libglade-2.3.6
	>=media-libs/libart_lgpl-2.3.11

	gconf? ( >=gnome-base/gconf-2 )
	gnome? (
		>=gnome-base/libgnome-2
		>=gnome-base/libgnomeui-2
		>=gnome-base/libbonobo-2.2
		>=gnome-base/libbonoboui-2.2 )
	perl? ( dev-lang/perl )
	python? (
		>=dev-lang/python-2
		>=dev-python/pygtk-2 )"
	# libgda? (
	#	>=gnome-extra/libgda-4.1.1
	#	>=gnome-extra/libgnomedb-3.99.6 )

DEPEND="${RDEPEND}
	>=dev-util/intltool-0.35
	>=dev-util/pkgconfig-0.18
	app-text/scrollkeeper"

DOCS="AUTHORS BEVERAGES BUGS ChangeLog HACKING MAINTAINERS NEWS README TODO"

pkg_setup() {
	local will_die=false

	G2CONF="${G2CONF}
		--enable-ssindex
		--enable-static
		--without-gda
		--without-guile
		--without-mono
		$(use_with perl)
		$(use_with python)
		$(use_with gnome)
		$(use_with gconf)"

	# gcc bug (http://bugs.gnome.org/show_bug.cgi?id=128834)
	replace-flags "-Os" "-O2"
}

src_install() {
	gnome2_src_install

	# make gnumeric find its help
	dosym \
		/usr/share/gnome/help/gnumeric \
		/usr/share/${PN}/${PV}/doc
}
